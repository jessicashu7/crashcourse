import java.util.*;


/**
 * Defines the characteristics of a car.
 * 
 * Includes a queue data structure which determines the order of which cars to
 * go first at the four-way stop sign intersection.
 * 
 * Extends the Obstacle class.
 *
 * @author Jessica Shu
 * @author Cassandra Overney
 * @version May 23, 2016
 * @author Period: 5
 * @author Assignment: CrashCourse
 *
 * @author Sources: None
 */
public class Car extends Obstacle
{
    private boolean mustStopAtSign;

    private static Queue<Car> queue;

    private int timer;

    private boolean hasStopped;

    private boolean startTimer;


    /**
     * Creates a Car.
     * 
     * Calls the superclass's constructor and initiates the queue and other
     * instance variables.
     * 
     * @param advice
     *            the message that will popup when a mistake occurs
     * 
     * @author Jessica Shu
     */
    public Car( String advice )
    {
        super( advice );
        mustStopAtSign = false;
        hasStopped = false;
        startTimer = false;
        timer = 150;
        queue = new LinkedList<Car>();
    }


    /**
     * 
     * 
     * Checks to see if the car is in the queue and acts appropriately. It also
     * starts the timer if boolean starTimer is true. It calls the superclass's
     * (Obstacle) act method.
     * @author Jessica Shu
     */
    public void act()
    {

        if ( startTimer )
        {
            Timer();
        }
        checkCarInQueue();
        super.act();

    }


    /**
     * Checks to see if the car has stopped or not.
     * 
     * @return true if car has stopped; otherwise, false.
     * @author Jessica Shu
     */
    public boolean hasStopped()
    {
        return hasStopped;
    }


    /**
     * Sets the variable "hasStopped" to true or false, depending on the user's
     * input.
     * 
     * 
     * @param stop
     *            true if the car has stopped; otherwise, false.
     * @author Jessica Shu
     */
    public void setHasStopped( boolean stop )
    {
        hasStopped = stop;
    }


    /**
     * Adds the car to the queue.
     * 
     * Sets up the timer (in this case, we set it to 150).
     * 
     * @author Jessica Shu
     */
    public void addCarToQueue()
    {
        queue.add( this );
        // System.out.println("add to queue/start queue timer: " + this);
        timer = 150;
        startTimer = true;

    }


    /**
     * Puts the timer to use. The "time" will decrease until it hits 0; when it
     * hits 0, the timer will be reset.
     * 
     * @author Jessica Shu
     */
    public void Timer()
    {
        if ( timer >= 0 )
        {
            timer--;
        }
        else if ( timer < 0 )
        {
            startTimer = false;
            // System.out.println("end queue timer: " + this);

        }
    }


    /**
     * Checks to see if the car can leave the queue. If the timer has finished,
     * the Car is first in the queue, mustStopAtSign is true, and hasStopped is
     * false, then sets MustStopAtSign to false and HasStopped to true.
     * 
     * @author Jessica Shu
     */
    public void checkCarInQueue()
    {
        if ( timer < 0 && isFirstInQueue() && this.mustStopAtSign() && !this.hasStopped() )
        {
            this.setMustStopAtSign( false );
            this.setHasStopped( true );
            if ( this.getClass() == MyCar.class )
            {
                showAdvice1();
                // System.out.println("show GO");
                // queue.remove();}
            }
        }
    }


    /**
     * Displays the GO! image by creating a text object
     * 
     * @author Cassandra Overney
     */
    public void showAdvice1()
    {

        Text msg = new Text( false, true, true );
        msg.setImage( "Go!.png" );
        msg.getImage().setTransparency( 250 );
        this.getWorld().addObject( msg, 361, 507 );
        msg.startTimer();

    }


    /**
     * Checks to see if queue is empty.
     * 
     * @return true if queue is empty; otherwise, false.
     * @author Jessica Shu
     */
    public static boolean queueIsEmpty()
    {
        return queue.isEmpty();
    }


    /**
     * Empties the queue of cars.
     * 
     * @author Jessica Shu
     */
    public static void emptyQueue()
    {
        while ( !queue.isEmpty() )
        {
            queue.remove();
        }
    }


    /**
     * Checks to see if this Car is first in the queue.
     * 
     * @return true if the car is first in queue; otherwise, false.
     * @author Jessica Shu
     */
    public boolean isFirstInQueue()
    {
        return queue.peek() == this;
    }


    /**
     * Removes Car from queue if it is first in the queue.
     * 
     * @return true if car has successfully been removed from the queue;
     *         otherwise, false.
     * @author Jessica Shu
     */
    public boolean removeCar()
    {
        if ( queue.peek() == this )
        {
            queue.remove();
            // System.out.println("remove from queue: " + this);
            return true;
        }
        return false;
    }


    /**
     * Sets mustStopAtSign to true or false, depending on the user's input.
     * 
     * @param stop
     *            true if car must stop at sign; otherwise, false.
     * @author Jessica Shu
     */
    public void setMustStopAtSign( boolean stop )
    {
        mustStopAtSign = stop;
    }


    /**
     * Tells if the car must stop at the stop sign or not.
     * 
     * @return true if car must stop at the stop sign; otherwise, false.
     * @author Jessica Shu
     */
    public boolean mustStopAtSign()
    {
        return mustStopAtSign;
    }

}