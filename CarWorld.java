import greenfoot.*; // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.*;


/**
 * Defines the world that the car can traverse through.
 * 
 * Extends the World class from Greenfoot.
 *
 * @author Alina Ying
 * @author Jessica Shu
 * @version April 25, 2016
 * @author Period: 5
 * @author Assignment: CrashCourse
 *
 * @author Sources: n/a
 */
public class CarWorld extends World
{
    private OtherCar left;

    private OtherCar right;

    private OtherCar top;

    private OtherCar bottom;

    private Intersection intersection;

    private boolean gameover;


    /**
     * 
     * Creates a CarWorld. Sets the world's background to the chosen image and
     * sets the car world's dimensions to the background image's dimensions.
     * Sets speed to 50 and calls createCarWorld().
     * 
     * @author Alina Ying
     */
    public CarWorld()
    {
        super( 719, 1051, 1, false );
        gameover = false;
        setBackground( "Car Course 2 (2).jpg" );
        Greenfoot.setSpeed( 50 );
        createCarWorld();
    }


    /**
     * 
     * 
     * Adds all the actors onto the CarWorld.
     * 
     * @author Alina Ying
     */
    public void createCarWorld()
    {
        // intersection with stop sign functions
        intersection = new Intersection();
        this.addObject( intersection, 357, 553 );

        // user's car
        Text dummyCar = new Text( false, false, false );
        dummyCar.setImage( "bluecar.png" );
        this.addObject( dummyCar, 46, 73 );
        dummyCar.setRotation( 90 );

        // stop signs
        StopSign leftsign = new StopSign( "Hit Stop Sign.png", "No Stop.png", 35, "left" );
        StopSign rightsign = new StopSign( "Hit Stop Sign.png", "No Stop.png", 35, "right" );
        StopSign topsign = new StopSign( "Hit Stop Sign.png", "No Stop.png", 35, "back" );
        StopSign bottomsign = new StopSign( "Hit Stop Sign.png", "No Stop.png", 35, "front" );
        this.addObject( leftsign, 282, 631 );
        this.addObject( rightsign, 436, 475 );
        this.addObject( topsign, 282, 475 );
        this.addObject( bottomsign, 436, 631 );

        // other cars
        right = new OtherCar( "Hit Car.png", 180 );
        // right.setInitialLocation(this.getWidth() +
        // right.getImage().getWidth(), 522);
        this.addObject( right, this.getWidth() + right.getImage().getWidth(), 522 );
        right.act();

        left = new OtherCar( "Hit Car.png", 0 );
        // left.setInitialLocation(-left.getImage().getWidth(), 590);
        this.addObject( left, -left.getImage().getWidth(), 590 );
        left.act();

        bottom = new OtherCar( "Hit Car.png", 270 );
        // bottom.setInitialLocation(388, this.getHeight() +
        // bottom.getImage().getHeight());
        this.addObject( bottom, 385, this.getHeight() + bottom.getImage().getHeight() );
        bottom.act();

        top = new OtherCar( "Hit Car.png", 90 );
        // top.setInitialLocation(322, -top.getImage().getHeight());
        this.addObject( top, 322, -top.getImage().getHeight() );
        top.act();

        // steering wheel
        Text dummyWheel = new Text( false, false, false );
        dummyWheel.setImage( "steering wheel.png" );
        dummyWheel.getImage().scale( 150, 150 );
        this.addObject( dummyWheel, 601, 955 );

        // Obstacles
        // Obstacles - first grass block
        for ( int y = 250; y <= 450; y += 100 )
        {
            Obstacle house1 = new Obstacle( "Hit Hotel.png", 1 );
            house1.setImage( "building 1.png" );
            house1.getImage().scale( 50, 50 );
            this.addObject( house1, 190, y );
            house1.act();

            Obstacle house2 = new Obstacle( "Hit Hotel.png", 1 );
            // house2 = new Obstacle( "Hotel" );
            house2.setImage( "building 2.png" );
            house2.getImage().scale( 50, 50 );
            this.addObject( house2, 250, y );
            house2.act();
        }

        // Obstacles - second grass block
        for ( int y = 190; y <= 350; y += 50 )
        {
            Obstacle house1 = new Obstacle( "Hit Building.png", 1 );
            house1.setImage( "building 4.png" );
            house1.getImage().scale( 50, 50 );
            this.addObject( house1, 470, y );
            house1.act();

            Obstacle house2 = new Obstacle( "Hit Building.png", 1 );
            // house2 = new Obstacle( "Hotel" );
            house2.setImage( "building 4.png" );
            house2.getImage().scale( 50, 50 );
            this.addObject( house2, 530, y );
            house2.act();
        }

        // Obstacles - third grass block
        Obstacle houseBunch = new Obstacle( "Hit Building.png", 2 );
        houseBunch.setImage( "bunch of houses.png" );
        houseBunch.getImage().scale( 130, 250 );
        this.addObject( houseBunch, 220, 750 );
        houseBunch.act();

        // Obstacles - fourth grass block
        Obstacle school = new Obstacle( "Hit School.png", 2 );
        school.setImage( "school.png" );
        school.getImage().scale( 100, 50 );
        this.addObject( school, 500, 680 );
        school.act();

        Obstacle park = new Obstacle( "Hit Park.png", 2 );
        park.setImage( "park.png" );
        park.getImage().scale( 120, 100 );
        this.addObject( park, 501, 840 );
        park.act();

        // Obstacles - on the road
        Obstacle leafPile = new Obstacle( "Hit Leaves.png", 1 );
        leafPile.setImage( "pile of leaves.png" );
        leafPile.getImage().scale( 30, 30 );
        this.addObject( leafPile, 570, 212 );
        leafPile.act();

        Obstacle fallenTree = new Obstacle( "Hit Log.png", 2 );
        fallenTree.setImage( "fallen tree.png" );
        fallenTree.getImage().scale( 50, 30 );
        this.addObject( fallenTree, 570, 747 );

        fallenTree.act();

        // Obstacles - random places in the car world to make it look better
        Obstacle house3 = new Obstacle( "Hit Building.png", 1 );
        house3.setImage( "building 5.png" );
        house3.getImage().scale( 50, 50 );
        this.addObject( house3, 250, 193 );
        house3.act();

        Obstacle house4 = new Obstacle( "Hit Building.png", 1 );
        house4.setImage( "building 5.png" );
        house4.getImage().scale( 50, 50 );
        this.addObject( house4, 190, 400 );
        house4.act();

        Obstacle house5 = new Obstacle( "Hit Building.png", 1 );
        house5.setImage( "building 3.png" );
        house5.getImage().scale( 50, 50 );
        this.addObject( house5, 470, 430 );

        house5.act();

        Obstacle house6 = new Obstacle( "Hit Building.png", 1 );
        house6.setImage( "building 3.png" );
        house6.getImage().scale( 50, 50 );
        this.addObject( house6, 530, 430 );
        house6.act();

        // make labels
        Text begLabel = new Text( true, false, false );
        begLabel.setImage( "Start Simulation.png" );
        begLabel.getImage().setTransparency( 230 );
        this.addObject( begLabel, 357, 400 );
        Greenfoot.start();
        begLabel.act();

    }


    /**
     * If the user has "gamed over," it tells Greenfoot to stop the simulation.
     * 
     * @author Jessica Shu
     */
    public void act()
    {
        if ( gameover )
        {
            Greenfoot.stop();
        }
    }


    /**
     * Returns boolean gameover.
     * 
     * @return true if user has lost/"gamed over"; otherwise, false.
     * 
     * @author Alina Ying
     */
    public boolean gameover()
    {
        return gameover;
    }


    /**
     * This sets the gameover variable to true or false, depending on the user's
     * input.
     * 
     * @param g
     *            true if the user has lost/"gamed over"; otherwise, false.
     * 
     * @author Alina Ying
     */
    public void setGameOver( boolean g )
    {
        gameover = g;
    }


    /**
     * Resets the "other cars" (all cars that the user cannot control) and calls
     * their act methods.
     * 
     * @param one
     *            the first car
     * @param two
     *            the second car
     * @param three
     *            the third car
     * 
     * @author Jessica Shu
     */
    public void callOtherCars( String one, String two, String three )
    {

        // System.out.println("callothercars");

        Car.emptyQueue();
        List<String> carstocall = new ArrayList<String>();
        carstocall.add( one );
        carstocall.add( two );
        carstocall.add( three );

        // System.out.println(carstocall);
        int i = 0;

        while ( !carstocall.isEmpty() )
        {

            String s = carstocall.get( i );
            if ( s.equals( "left" ) )
            {

                left = new OtherCar( "Hit Car.png", 0 );
                // left.setInitialLocation(-left.getImage().getWidth(), 590);
                this.addObject( left, -left.getImage().getWidth(), 590 );
                left.setAct( true );
                left.act();

            }
            else if ( s.equals( "right" ) )
            {
                right = new OtherCar( "Hit Car.png", 180 );
                // right.setInitialLocation(this.getWidth() +
                // right.getImage().getWidth(), 522);
                this.addObject( right, this.getWidth() + right.getImage().getWidth(), 522 );
                right.setAct( true );
                right.act();

            }
            else if ( s.equals( "top" ) )
            {
                top = new OtherCar( "Hit Car.png", 90 );
                // top.setInitialLocation(322, -top.getImage().getHeight());
                this.addObject( top, 322, -top.getImage().getHeight() );
                top.setAct( true );
                top.act();
            }
            else if ( s.equals( "bottom" ) )
            {
                bottom = new OtherCar( "Hit Car.png", 270 );
                // bottom.setInitialLocation(388, this.getHeight() +
                // bottom.getImage().getHeight());
                this.addObject( bottom, 385, this.getHeight() + bottom.getImage().getHeight() );
                bottom.setAct( true );
                bottom.act();
            }
            carstocall.remove( i );
        }
    }

}