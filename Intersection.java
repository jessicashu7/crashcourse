import greenfoot.*;


/**
 * This class manages the "detection" function of the intersection in the car
 * world (located in the center of the car world).
 * 
 * The "other cars" (all cars that the user cannot control) only cross the
 * intersection when the user's car is close to the intersection. This class is
 * in charge of "detecting" when the "other cars" should come and coding when
 * the other cars cross, accordingly. In addition, when a car leaves the
 * intersection, it removes the car from the queue so that the next car can
 * cross.
 * 
 * The intersection class extends the Actor class.
 *
 * @author Cassandra Overney
 * @author Jessica Shu
 * @version April 25, 2016
 * @author Period: 5
 * @author Assignment: CrashCourse
 *
 * @author Sources: n/a
 */
public class Intersection extends Actor
{

    private boolean myCarIsComing;


    /**
     * Creates an Intersection.
     * 
     * Sets myCarIsComing to true.
     * 
     * 
     * @author Jessica Shu
     */
    public Intersection()
    {
        myCarIsComing = true;
        // carIsLeaving = false;
        // timer = -1;
    }


    /**
     * 
     * If myCarIsComing, calls myCarIsComing. It also calls carIsLeaving.
     * 
     * @author Cassandra Overney
     */
    public void act()
    {

        if ( myCarIsComing )
        {
            MyCarIsComing();
            // start timer

        }
        carIsLeaving();

    }


    /**
     * Sets boolean "myCarIsComing" to true or false.
     * 
     * @param coming
     *            true if the user's car is coming; otherwise, false.
     * @author Jessica Shu
     */
    public void setMyCarIsComing( boolean coming )
    {
        myCarIsComing = coming;
    }


    /**
     * Checks if MyCar is approaching the intersection. If it is, it calls the
     * OtherCars to come and sets MyCarIsComing to false.
     * 
     * @author Jessica Shu
     */
    public void MyCarIsComing()
    {

        // this.getWorld().showText( "i",
        // this.getX() + this.getImage().getWidth(),
        // this.getY() - this.getImage().getHeight() / 4 );
        // this.getWorld().showText( "i",
        // this.getX() + this.getImage().getWidth() / 5,
        // (int)( this.getY() + 2.5 * this.getImage().getHeight() ) );
        // this.getWorld().showText( "i",
        // this.getX() - this.getImage().getWidth(),
        // this.getY() + this.getImage().getHeight() / 4 );
        // this.getWorld().showText( "i",
        // this.getX() - this.getImage().getWidth() / 5,
        // (int)( this.getY() - 2.5 * this.getImage().getHeight() ) );

        MyCar car = (MyCar)this.getOneObjectAtOffset( this.getImage().getWidth(),
            -this.getImage().getHeight() / 4,
            MyCar.class );
        if ( car != null )
        {

            this.setMyCarIsComing( false );

            // System.out.println("set my car is coming: false");

            car.setHasStopped( false ); // added
            ( (CarWorld)this.getWorld() ).callOtherCars( "left", "top", "bottom" );

        }
        MyCar car2 = (MyCar)this.getOneObjectAtOffset( this.getImage().getWidth() / 5,
            (int)( 2.5 * this.getImage().getHeight() ),
            MyCar.class );
        if ( car2 != null )

        {
            this.setMyCarIsComing( false );

            // System.out.println("set my car is coming: false");

            car2.setHasStopped( false ); // added
            ( (CarWorld)this.getWorld() ).callOtherCars( "left", "top", "right" );

        }
        MyCar car3 = (MyCar)this.getOneObjectAtOffset( -this.getImage().getWidth(),
            this.getImage().getHeight() / 4,
            MyCar.class );
        if ( car3 != null )
        {
            this.setMyCarIsComing( false );

            // System.out.println("set my car is coming: false");

            car3.setHasStopped( false ); // added
            ( (CarWorld)this.getWorld() ).callOtherCars( "right", "top", "bottom" );

        }
        MyCar car1 = (MyCar)this.getOneObjectAtOffset( -this.getImage().getWidth() / 5,
            (int)( -2.5 * this.getImage().getHeight() ),
            MyCar.class );
        if ( car1 != null )
        {
            this.setMyCarIsComing( false );

            // System.out.println("set my car is coming: false");

            car1.setHasStopped( false ); // added
            ( (CarWorld)this.getWorld() ).callOtherCars( "left", "right", "bottom" );

        }

    }


    /**
     * Checks if a Car is leaving and removes it from the queue. If it is
     * successfully removed, sets myCarIsComing to true.
     * 
     * @author Jessica Shu
     */
    public void carIsLeaving()
    {
        // this.getWorld().showText( "i",
        // (int)( this.getX() + this.getImage().getWidth() / 1.5 ),
        // this.getY() + this.getImage().getHeight() / 4 );
        // this.getWorld().showText( "i",
        // this.getX() - this.getImage().getWidth() / 5,
        // (int)( this.getY() + this.getImage().getHeight() ) );
        // this.getWorld().showText( "i",
        // (int)( this.getX() - this.getImage().getWidth() / 1.5 ),
        // this.getY() - this.getImage().getHeight() / 4 );
        // this.getWorld().showText( "i",
        // this.getX() + this.getImage().getWidth() / 5,
        // (int)( this.getY() - this.getImage().getHeight() ) );

        Car car = (Car)this.getOneObjectAtOffset( (int)( this.getImage().getWidth() / 1.5 ),
            this.getImage().getHeight() / 4,
            Car.class );
        if ( car != null )
        {
            boolean c = car.removeCar();
            if ( Car.queueIsEmpty() && c )
            {
                // System.out.println("set my car is coming: true");

                this.setMyCarIsComing( true );

            }
        }
        Car car2 = (Car)this.getOneObjectAtOffset( -this.getImage().getWidth() / 5,
            this.getImage().getHeight(),
            Car.class );
        if ( car2 != null )

        {
            boolean c = car2.removeCar();
            if ( Car.queueIsEmpty() && c )
            {
                this.setMyCarIsComing( true );
                // System.out.println("set my car is coming: true");

            }
        }
        Car car3 = (Car)this.getOneObjectAtOffset( (int)( -this.getImage().getWidth() / 1.5 ),
            -this.getImage().getHeight() / 4,
            Car.class );
        if ( car3 != null )
        {
            boolean c = car3.removeCar();
            if ( Car.queueIsEmpty() && c )
            {
                this.setMyCarIsComing( true );
                // System.out.println("set my car is coming: true");

            }
        }

        Car car1 = (Car)this.getOneObjectAtOffset( this.getImage().getWidth() / 5,
            -this.getImage().getHeight(),
            Car.class );
        if ( car1 != null )
        {
            boolean c = car1.removeCar();
            if ( Car.queueIsEmpty() && c )
            {
                this.setMyCarIsComing( true );
                // System.out.println("set my car is coming: true");

            }
        }
    }
}