import greenfoot.*; // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)


/**
 * Defines the user's car (the only car that the user can control).
 *
 * @author Alina Ying
 * @version April 25, 2016
 * @author Period: 5
 * @author Assignment: CrashCourse
 * 
 * @author Sources: n/a
 */
public class MyCar extends Car
{
    private int count;
    // private boolean gameover = false;


    /**
     * Creates a MyCar.
     * 
     * Calls the superclass's (Car) constructor with an empty string parameter.
     */
    public MyCar()
    {
        super( "" );
        count = 0;
        // this.getImage().rotate( 90 );
        // this.getImage().scale( 85, 40);
    }


    /**
     * Takes care of the car light and moving aspect. If the left turn key is
     * pressed, the left light will turn on; if the right turn key is pressed,
     * the right light will turn on; if the brake key is pressed, the brake
     * lights will turn on. If the user is driving forward, backwards, left, or
     * right, the lights will immediately turn off. Calls move method.
     * 
     * 
     */
    public void act()
    {
        super.act();
        // if ( ( (CarWorld)this.getWorld() ).gameover() )
        // return;

        if ( isLeftTurnSignalOn() )
        {
            turnFrontLeftLightOn();
        }

        if ( isRightTurnSignalOn() )
        {
            turnFrontRightLightOn();
        }

        // just normal brake
        if ( isBrakeDown() )
        {
            turnBrakesOn();
        }
        if ( isReturnToNormal() )
        {
            returnToNormal();
        }
        move();
    }


    /**
     * Makes sure the user's car moves depending on the appropriate keys: if the
     * user presses the "up" key or "up" and "space", the user will go up; if
     * the user presses the "space" key only, the user will stop (brake); if the
     * user presses the "down" key, the user will go backwards; if the user
     * presses the "right" key, the car will turn right; if the user presses the
     * "left" key, the user will turn left.
     */
    public void move()
    {
        if ( Greenfoot.isKeyDown( "up" ) && isBrakeDown() )
        {
            count--;
            move( 1 );
        }
        if ( Greenfoot.isKeyDown( "space" ) )
        {
            count = 0;
            return;
        }

        if ( Greenfoot.isKeyDown( "up" ) && ( isLeftTurnSignalOn() || isRightTurnSignalOn() ) )
        {
            count--;
        }

        if ( Greenfoot.isKeyDown( "down" ) && ( isLeftTurnSignalOn() || isRightTurnSignalOn() ) )
        {
            count--;
        }

        if ( Greenfoot.isKeyDown( "right" ) && ( isLeftTurnSignalOn() || isRightTurnSignalOn() ) )
        {
            count--;
        }

        if ( Greenfoot.isKeyDown( "left" ) && ( isLeftTurnSignalOn() || isRightTurnSignalOn() ) )
        {
            count--;
        }

        if ( Greenfoot.isKeyDown( "up" ) )
        {
            count--;
            move( 1 );
        }
        if ( Greenfoot.isKeyDown( "left" ) )
        {
            count--;
            turn( -1 );
        }
        if ( Greenfoot.isKeyDown( "right" ) )
        {
            count--;
            turn( 1 );
        }
        if ( Greenfoot.isKeyDown( "down" ) )
        {
            count--;
            move( -1 );
        }
    }

    // public void setGameOver( boolean over )
    // {
    // gameover = over;
    // }


    // the car light methods
    /**
     * Checks to see if the right turn signal key ("s") is pressed.
     * 
     * @return true if "s" is pressed; otherwise, false.
     */
    public boolean isRightTurnSignalOn()
    {
        if ( Greenfoot.isKeyDown( "s" ) )
        {
            count = 100;
            return true;
        }
        return false;

    }


    /**
     * Checks to see if the left turn signal key ("a") is pressed.
     * 
     * @return true if "a" is pressed; otherwise, false.
     */
    public boolean isLeftTurnSignalOn()
    {
        if ( Greenfoot.isKeyDown( "a" ) )
        {
            count = 100;
            return true;
        }
        return false;

    }


    /**
     * Checks to see if the brake key ("space") is pressed.
     * 
     * @return true if "space" is pressed; otherwise, false.
     */
    public boolean isBrakeDown()
    {
        if ( Greenfoot.isKeyDown( "space" ) )
        {
            return true;
        }
        return false;

    }


    /**
     * Checks to see if the car should "return to normal," as in have no lights
     * on.
     * 
     * The car should "return to normal" if the car is moving (up, down, right,
     * left).
     * 
     * note-the new image must be equal to or smaller than the original car's
     * image; otherwise, the car will risk crashing at the small instant between
     * its creation and its rescaling.
     * 
     * @return true if the user's car is moving; otherwise, false.
     */
    public boolean isReturnToNormal()
    {
        while ( Greenfoot.isKeyDown( "up" ) || Greenfoot.isKeyDown( "down" ) || Greenfoot.isKeyDown( "right" )
            || Greenfoot.isKeyDown( "left" ) )
        {
            return true;
        }
        return false;
    }


    /**
     * Tturns the front left light on by changing the car's image, which is a
     * replica of the original car except with the front left light on.
     * 
     * It then scales the image to approximately the same size as the original
     * car.
     * 
     * note-the new image must be equal to or smaller than the original car's
     * image; otherwise, the car will risk crashing at the small instant between
     * its creation and its rescaling.
     */
    public void turnFrontLeftLightOn()
    {
        setImage( "car left.png" );
        this.getImage().scale( 91, 41 );
    }


    /**
     * Turns the front right light on by changing the car's image, which is a
     * replica of the original car except with the front right light on.
     * 
     * It then scales the image to approximately the same size as the original
     * car.
     * 
     * note-the new image must be equal to or smaller than the original car's
     * image; otherwise, the car will risk crashing at the small instant between
     * its creation and its rescaling.
     */
    public void turnFrontRightLightOn()
    {
        setImage( "car right.png" );
        this.getImage().scale( 85, 41 );
    }


    /**
     * Turns the brake light on by changing the car's image, which is a replica
     * of the original car except with the brake light on.
     * 
     * It then scales the image to approximately the same size as the original
     * car.
     * 
     * note-the new image must be equal to or smaller than the original car's
     * image; otherwise, the car will risk crashing at the small instant between
     * its creation and its rescaling.
     */
    public void turnBrakesOn()
    {
        setImage( "car brake.png" );
        this.getImage().scale( 90, 45 );
    }


    /**
     * Turns all the lights off of the car by setting the car's image to the
     * original image.
     */
    public void returnToNormal()
    {
        if ( count == 0 )
        {
            setImage( "bluecar.png" );
        }
        // this.getImage().rotate(90);
        // this.getImage().scale( 85, 40 );
    }
}