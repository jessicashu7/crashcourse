import greenfoot.*; // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)


/**
 * Defines an obstacle.
 * 
 * Extends the Actor class from Greenfoot.
 *
 * @author Jessica Shu
 * @author Cassandra Overney
 * @version April 25, 2016
 * @author Period: 5
 * @author Assignment: CrashCourse
 *
 * @author Sources: n/a
 */
public class Obstacle extends Actor
{
    private String advice;

    private int typeBuilding;


    /**
     * 
     * Creates an Obstacle. Sets advice to the message that the user chooses.
     * 
     * @param msg
     *            the picture filename for the advice that corresponds to each
     *            type of obstacle.
     * @author Jessica Shu
     */
    public Obstacle( String msg )
    {
        advice = msg;
    }


    /**
     * Another constructor for the stationary obstacles only,
     * 
     * @param msg
     *            the picture filename for the advice that corresponds to each
     *            type of obstacle.
     * @param type
     *            the type of house depending on location
     * @author Cassandra Overney
     */
    public Obstacle( String msg, int type )
    {
        advice = msg;
        typeBuilding = type;

    }


    /**
     * Calls the checkCrash method.
     * 
     * @author Jessica Shu
     */
    public void act()
    {
        checkCrash();
    }


    /**
     * Checks if MyCar hits this Obstacle. If it does, calls crash method and
     * game over.
     * 
     * @author Jessica Shu
     */
    public void checkCrash()
    {
        MyCar car = (MyCar)this.getOneIntersectingObject( MyCar.class );
        if ( car != null )
        {
            crash();
            ( (CarWorld)this.getWorld() ).setGameOver( true );
        }

        // MyCar car = (MyCar)this.getOneObjectAtOffset(
        // this.getImage().getWidth() / 2 - 3,
        // this.getImage().getHeight() / 2 - 3,
        // MyCar.class );
        // if ( car != null )
        // {
        // ((CarWorld) this.getWorld()).setGameOver(true);
        //
        // //car.setGameOver( true );
        // crash();
        // }
        // MyCar car1 = (MyCar)this.getOneObjectAtOffset(
        // -this.getImage().getWidth() / 2 + 3,
        // -this.getImage().getHeight() / 2 + 3,
        // MyCar.class );
        // if ( car1 != null )
        // {
        // ((CarWorld) this.getWorld()).setGameOver(true);
        //
        // //car1.setGameOver( true );
        // crash();
        // }
        // MyCar car2 = (MyCar)this.getOneObjectAtOffset(
        // this.getImage().getWidth() / 2 - 3,
        // -this.getImage().getHeight() / 2 + 3,
        // MyCar.class );
        // if ( car2 != null )
        // {
        // ((CarWorld) this.getWorld()).setGameOver(true);
        //
        // //car2.setGameOver( true );
        // crash();
        // }
        // MyCar car3 = (MyCar)this.getOneObjectAtOffset(
        // -this.getImage().getWidth() / 2 + 3,
        // this.getImage().getHeight() / 2 - 3,
        // MyCar.class );
        // if ( car3 != null )
        // {
        // ((CarWorld) this.getWorld()).setGameOver(true);
        //
        // // car3.setGameOver( true );
        // crash();
        // }

    }


    /**
     * Shows advice from the obstacle in the car world when a crash occurs by
     * creating a Text object and displaying the advice image (obtained from the
     * advice field)
     * 
     * @author Cassandra Overney
     */
    public void crash()
    {
        // if type is 0 then put message on top, if type is 1 then put message
        // on buttom. If no type == null, just put it in the middle

        Text msg = new Text( false, false, false );
        msg.setImage( advice );
        msg.getImage().setTransparency( 230 );

        if ( this.typeBuilding == 0 )
        {
            this.getWorld().addObject( msg, 363, 507 );
        }
        else if ( this.typeBuilding == 1 )
        {

            this.getWorld().addObject( msg, 363, 319 );
        }

        else if ( this.typeBuilding == 2 )
        {
            this.getWorld().addObject( msg, 363, 616 );
        }

        // getWorld().showText( advice,
        // this.getX(),
        //
        // this.getY() + this.getImage().getHeight() );
    }

    // /**
    // * Gets the advice.
    // *
    // * @return advice the advice
    // * @author Jessica Shu
    // */
    // public String getAdvice()
    // {
    // return advice;
    // }

}