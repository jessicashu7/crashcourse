
/**
 * Defines an "other car" (all cars that the user cannot control).
 * 
 * Extends the Car class.
 *
 * @author Jessica Shu
 * @version April 25, 2016
 * @author Period: 5
 * @author Assignment: CrashCourse
 *
 * @author Sources: n/a
 */
public class OtherCar extends Car
{
    // private int initialX;

    // private int initialY;

    private int rotation;

    private boolean act;


    /**
     * Creates an OtherCar.
     * 
     * Sets the advice message, initial position (with x and y coordinates), and
     * rotation. Sets boolean act to false.
     * 
     * @param advice
     *            the piece of advice that appears when the user's car crashes
     *            into it
     * @param rotation
     *            the direction the other car faces
     */
    public OtherCar( String advice, int rotation )
    {
        super( advice );
        // initialX = x;
        // initialY = y;
        this.rotation = rotation;
        setRotation( rotation );
        act = false;
    }

    // /**
    // * Resets the other car to its original starting point so it is ready to
    // act
    // * again.
    // */
    // public void reset()
    // {
    // // System.out.println( "reset" );
    // setLocation( initialX, initialY );
    // this.setMustStopAtSign( false );
    // this.setHasStopped( false );
    // this.setAct( true );
    //
    // }


    // /**
    // * sets
    // *
    // * @param x
    // * x coord
    // * @param y
    // * y coord
    // */
    // public void setInitialLocation( int x, int y )
    // {
    // initialX = x;
    // initialY = y;
    // }

    /**
     * Sets boolean act.
     * 
     * @param act
     *            true if the other car should act; otherwise, false.
     */
    public void setAct( boolean act )
    {
        this.act = act;
    }


    /**
     * Moves (when act is true) across the intersection and stops when it is at
     * the stop sign or if it is off the map.
     */
    public void act()
    {
        if ( act )
        {
            super.act();
            if ( !mustStopAtSign() )
            {
                // System.out.println( "other car act" );

                this.move( 1 );

                if ( rotation == 0 )
                {
                    if ( this.getX() > this.getWorld().getWidth() + this.getImage().getWidth() )
                    {
                        this.setMustStopAtSign( false );
                        this.setHasStopped( false );
                        act = false;
                        // System.out.println( "stopped at right end of map" );
                    }

                }
                else if ( rotation == 90 )
                {
                    if ( this.getY() > this.getWorld().getHeight() + this.getImage().getHeight() )
                    {
                        this.setMustStopAtSign( false );
                        this.setHasStopped( false );
                        act = false;
                        // System.out.println( "stopped at top end of map" );

                    }

                }
                else if ( rotation == 180 )
                {
                    if ( this.getX() < -this.getWorld().getWidth() - this.getImage().getWidth() )
                    {
                        this.setMustStopAtSign( false );
                        this.setHasStopped( false );
                        act = false;
                        // System.out.println( "stopped at left end of map" );
                    }

                }
                else if ( rotation == 270 )
                {
                    if ( this.getY() < -this.getWorld().getHeight() - this.getImage().getHeight() )
                    {
                        this.setMustStopAtSign( false );
                        this.setHasStopped( false );
                        act = false;
                        // System.out.println( "stopped at bottom end of map" );

                    }

                }
            }
        }
    }
}