import greenfoot.*; // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)


/**
 * Defines the steering wheel icon and its movements.
 * 
 * It is located at the bottom-right corner of the car world. It models a real
 * steering wheel. When the car turns, the steering wheel also turns. There is
 * also a timer that moves the steering wheel back to its original position
 * after the timer is finished.
 * 
 * The SteeringWheel class extends the Actor class from Greenfoot.
 *
 * @author Alina Ying
 * @author Cassandra Overney
 * @version April 25, 2016
 * @author Period: 5
 * @author Assignment: CrashCourse
 *
 * @author Sources: None
 */
public class SteeringWheel extends Actor
{
    private int timer;

    private int rotation;


    /**
     * Creates a SteeringWheel.
     * 
     * Sets the image to a certain size using the scale method (150, 150). Sets
     * the timer and rotation variable to -1 and 0, respectively. The rotation
     * and timer variables are used to adjust the steering wheel back to its
     * original position after a turn is made.
     * 
     * @author Cassandra Overney
     */
    public SteeringWheel()
    {
        this.getImage().scale( 150, 150 );
        timer = -1;
        rotation = 0;
    }


    /**
     * 
     * 
     * Tests to see if the left or right key is pressed and turns the steering
     * wheel image to the respective direction (left key turns the steering
     * wheel left; right key turns the steering wheel right). Depending on the
     * direction of the wheel, the rotation variable either increases by 2
     * (right) or decreases by 2 (left). There is also a timer feature (in the
     * form of an if statement that decrements the timer by 1 each time it is
     * executed). Once the timer variable value is back to zero, it turns the
     * steering wheel back to its normal position (turns to -1 * rotation).
     * 
     * @author Alina Ying (turning and user)
     * @author Cassandra Overney (timer and adjustment to normal)
     */
    public void act()
    {
        if ( Greenfoot.isKeyDown( "left" ) )
        {
            turn( -2 );
            rotation = rotation - 2;
            timer = 100;

        }
        if ( timer > 0 )
        {
            timer--;

        }

        if ( timer == 0 )
        {

            turn( -1 * rotation );
            rotation = 0;
            timer = -1;
        }

        if ( Greenfoot.isKeyDown( "right" ) )
        {
            turn( 2 );
            rotation = rotation + 2;
            timer = 100;
        }

    }
}
