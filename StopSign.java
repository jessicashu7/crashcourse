import greenfoot.*; // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)


/**
 * Represents a stop sign.
 *
 * @author Jessica Shu
 * @author Cassandra Overney
 * @version April 25, 2016
 * @author Period: 5
 * @author Assignment: CrashCourse
 *
 * @author Sources: n/a
 */
public class StopSign extends Obstacle
{
    private String position;

    private int range;

    private String advice2; // first advice is crash, advice2 is if user doesn't
    // press the correct buttons

    private int timer;

    private Car car;
    // private int timer2;

    private boolean two;
    // private boolean one;


    /**
     * Calls the super constructor (Obstacle)for the message.
     * 
     * Scales the image down to an appropriate size (20, 30).
     * 
     * Sets the timer to -1 initially.
     * 
     * @param msg
     *            the "crash" advice that is associated with all obstacles
     * @param msg2
     *            the advice that is specific for Stop Sign if MyCar doesn't
     *            stop correctly
     * @param range
     *            how long the car should stop at the stop sign
     * @param position
     *            stop sign position: left, right, back, or front
     * 
     * @author Jessica Shu
     */
    public StopSign( String msg, String msg2, int range, String position )
    {
        super( msg );
        advice2 = msg2;
        this.range = range;
        this.position = position;
        this.getImage().scale( 20, 30 );
        timer = -1;
        // one = true;
        two = true;
        car = null;
    }


    // /**
    // * Gets the advice #2 (the advice that is specific for
    // * Stop Sign).
    // *
    // * @return the advice message #2
    // *
    // * @author Cassandra Overney
    // */
    // public String getAdvice2()
    // {
    // return advice2;
    // }

    /**
     * Returns the Car that is at the stop sign line.
     * 
     * @return Car if the Car is nearby StopSign; otherwise, null.
     * 
     * @author Jessica Shu detection and calling other methods
     * @author Cassandra Overney text messages
     */
    public Car CarIsAtStopSign()
    {

        if ( this.position.equals( "back" ) )
        {
            Car car = (Car)this.getOneObjectAtOffset( this.getImage().getWidth() / 2 + range,
                this.getImage().getHeight() / 2,
                Car.class );

            // this.getWorld().showText("s",
            // this.getX()+this.getImage().getWidth() / 2 + range,
            // this.getY()+this.getImage().getHeight() / 2);

            if ( car != null && !car.mustStopAtSign() && !car.hasStopped() )
            {
                car.setMustStopAtSign( true );
                car.addCarToQueue();

                if ( car.getClass() == MyCar.class )
                {
                    Text msg = new Text( false, true, false );
                    msg.setImage( "STOP!.png" );
                    msg.getImage().setTransparency( 250 );
                    this.getWorld().addObject( msg, 361, 507 );
                    msg.startTimer();
                }
                return car;

            }
        }
        else if ( this.position.equals( "front" ) )
        {

            // this.getWorld().showText("s",
            // this.getX()-this.getImage().getWidth()/2 - range,
            // this.getY()+this.getImage().getHeight() / 2);

            Car car2 = (Car)this.getOneObjectAtOffset( -this.getImage().getWidth() / 2 - range,
                this.getImage().getHeight() / 2,
                Car.class );
            if ( car2 != null && !car2.mustStopAtSign() && !car2.hasStopped() )
            {
                car2.setMustStopAtSign( true );
                car2.addCarToQueue();

                if ( car2.getClass() == MyCar.class )
                {
                    Text msg = new Text( false, true, false );
                    msg.setImage( "STOP!.png" );
                    msg.getImage().setTransparency( 250 );
                    this.getWorld().addObject( msg, 361, 507 );
                    msg.startTimer();
                }
                return car2;
            }
        }
        else if ( this.position.equals( "right" ) )
        {

            // this.getWorld().showText("s", this.getX(), this.getY()+
            // this.getImage().getHeight() / 2 + range);

            Car car1 = (Car)this.getOneObjectAtOffset( 0, this.getImage().getHeight() / 2 + range, Car.class );

            if ( car1 != null && !car1.mustStopAtSign() && !car1.hasStopped() )
            {
                car1.setMustStopAtSign( true );
                car1.addCarToQueue();

                if ( car1.getClass() == MyCar.class )
                {
                    Text msg = new Text( false, true, false );
                    msg.setImage( "STOP!.png" );
                    msg.getImage().setTransparency( 250 );
                    this.getWorld().addObject( msg, 361, 507 );
                    msg.startTimer();
                }

                return car1;
            }
        }
        else if ( this.position.equals( "left" ) )
        {

            // this.getWorld().showText("s", this.getX(), this.getY()
            // -this.getImage().getHeight() / 2 - range);

            Car car3 = (Car)this.getOneObjectAtOffset( 0, -this.getImage().getHeight() / 2 - range, Car.class );

            if ( car3 != null && !car3.mustStopAtSign() && !car3.hasStopped() )
            {
                car3.setMustStopAtSign( true );
                car3.addCarToQueue();

                if ( car3.getClass() == MyCar.class )
                {
                    Text msg = new Text( false, true, false );
                    msg.setImage( "STOP!.png" );
                    msg.getImage().setTransparency( 250 );
                    this.getWorld().addObject( msg, 361, 507 );
                    msg.startTimer();
                }
                return car3;

            }
        }
        return null;

    }


    // /**
    // * Gets the range.
    // *
    // * @return the range
    // *
    // * @author Jessica Shu
    // */
    // public int getRange()
    // {
    // return range;
    // }

    /**
     * 
     * 
     * Controls the timer for the stop sign and makes sure each car follows the
     * "rules" of driving by stopping at the stop signs.
     * 
     * The timer only runs when there is a car at the stop sign
     * 
     * It calls the superclass's act method.
     * 
     * @author Cassandra Overney
     */
    public void act()
    {
        Car c = CarIsAtStopSign();
        if ( c != null )
        {
            this.car = c;

            if ( car.getClass() == MyCar.class )
            {

                timer = 35;
                this.setTwo( true );
            }
        }
        if ( timer > 0 )
        {
            timer--;

        }
        if ( timer == 0 )
        {
            // System.out.println( "check user fault" );
            checkUserFault();

        }

        super.act();
    }


    /**
     * Checks the user's car and sees if the user has responsibly stopped at the
     * stop sign it is in front of.
     * 
     * @return true if the user has suitably stopped/braked; otherwise, false.
     * 
     * @author Jessica Shu
     */
    public boolean checkUserFault()
    {

        // System.out.println( "checking user" );

        if ( this.car != null )
        {
            // System.out.println(car + " must stop at stop sign: " +
            // car.mustStopAtSign());
            // System.out.println(car + " has stopped: " + car.hasStopped());
            // System.out.println(car + " first in queue: " +
            // car.isFirstInQueue());

            if ( car.getClass() == MyCar.class && ( !Greenfoot.isKeyDown( "space" ) || Greenfoot.isKeyDown( "up" ) )
                && this.car.mustStopAtSign() && !this.car.hasStopped() )
            {// &&
             // System.out.println("timer= " + timer);

                // System.out.println("no pass");
                showAdvice2();

                // System.out.println("show advice 2");
                // System.out.println(car);
                two = false;
                return true;
            }
            if ( car.getClass() == MyCar.class && !this.car.mustStopAtSign() && this.car.hasStopped() )
            {
                timer = -1;
                // System.out.println("stop checking user");
            }
        }
        // System.out.println(car);
        // System.out.println( "pass" );
        return false;
    }


    /**
     * Sets boolean two to parameter.
     * 
     * @param calltwo
     *            boolean to set two to
     * @author Jessica Shu
     */
    public void setTwo( boolean calltwo )
    {
        this.two = calltwo;
    }


    /**
     * Shows advice #2 (the advice that is specific for Stop Sign) and sets the
     * gameover boolean to true
     * 
     * @author Cassandra Overney
     */
    public void showAdvice2()
    {
        // getWorld().showText( advice2,
        // this.getX(),
        // this.getY() + this.getImage().getHeight() );
        if ( two )
        {

            Text msg = new Text( false, false, false );
            msg.setImage( advice2 );
            msg.getImage().setTransparency( 230 );
            this.getWorld().addObject( msg, 361, 507 );

            ( (CarWorld)this.getWorld() ).setGameOver( true );
        }
    }

}