import greenfoot.*;
// import java.awt.Color;
// import java.awt.Dimension;
// import java.awt.Font;
import java.util.List;


/**
 * Displays the text boxes when the user does something wrong. Also displays the
 * text box at the beginning of the simulation to display the rules and stuff.
 * Displays a text box at the end of the simulation after the user makes a
 * mistake.
 *
 * @author Cassandra Overney
 * @version May 27, 2016
 * @author Period: 5
 * @author Assignment: CrashCourse
 *
 * @author Sources: None
 */
public class Text extends Actor
{
    private boolean isStart;

    private boolean isTemp;

    private boolean isGo;

    private int timer;


    /**
     * The constructor for the Text class
     * 
     * @param start
     *            boolean which determines whether the label is the start
     *            simulation label
     * @param temp
     *            boolean which determines whether the label is the "pass" or
     *            "Car is at stop sign" label which only appears for a certain
     *            amount of time
     * @param go
     *            boolean that determines whether the label is specifically the
     *            "pass" label
     */
    public Text( boolean start, boolean temp, boolean go )
    {
        isStart = start;
        isTemp = temp;
        isGo = go;
        timer = -1;
    }


    /**
     * If the label is the start simulation label, then it wait until the enter
     * key is pressed, then disappears. If the label is the "pass" or
     * "car is at stop sign" label then the act method has a timer that runs and
     * then the message disappears
     */
    public void act()
    {
        if ( isStart )
        {

            if ( Greenfoot.isKeyDown( "enter" ) )
            {

                List<Text> labels = this.getWorld().getObjects( Text.class );
                while ( !labels.isEmpty() )
                {
                    Text label = labels.remove( 0 );
                    if ( !label.isStart() )
                    {
                        this.getWorld().removeObject( label );
                    }
                }
                this.getImage().clear();
                MyCar car = new MyCar();
                this.getWorld().addObject( car, 46, 73 );
                car.setRotation( 90 );
                car.act();

                List<Text> wheels = this.getWorld().getObjects( Text.class );
                while ( !wheels.isEmpty() )
                {
                    Text wheel = wheels.remove( 0 );
                    if ( !wheel.isStart() )
                    {
                        this.getWorld().removeObject( wheel );
                    }
                }
                SteeringWheel wheel = new SteeringWheel();
                this.getWorld().addObject( wheel, 601, 955 );
                wheel.act();

                isStart = false;
            }
        }

        if ( timer > 0 )
        {
            timer--;
        }

        if ( timer == 0 )
        {
            this.getImage().clear();
            timer = -1;
        }

    }


    /**
     * Sets the timer variable to a certain value in order to start the timer.
     * Of course this only happens if isTemp is true.
     */
    public void startTimer()
    {
        if ( isTemp )
        {
            if ( isGo )
            {
                timer = 20;
            }
            else
            {
                timer = 25;
            }
            // System.out.println( "timer start" );
        }
    }


    /**
     * Returns the boolean isStart.
     * 
     * @return boolean
     */
    public boolean isStart()
    {
        return isStart;
    }

}
